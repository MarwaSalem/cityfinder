//
//  HostService.swift
//  CityFinder
//
//  Created by Marwa Salem on 3/17/18.
//  Copyright © 2018 marwaSalem. All rights reserved.
//

import Foundation

struct HostService {
    
    static func getCityListBaseUrl() -> String {
        return "http://assignment.pharos-solutions.de/"
    }
    
    static func getGoogleMapsBaseUrl() -> String {
        return "https://maps.googleapis.com/maps/api/"
    }
    
    static func getGoogleMapsAPIKey() -> String {
        return "AIzaSyALaWq0949tvQKoYZJv2dc705IJFe83UjA"
    }
}
