//
//  AlamofireNetworkManager.swift
//  CityFinder
//
//  Created by Marwa Salem on 3/17/18.
//  Copyright © 2018 marwaSalem. All rights reserved.
//

import Foundation
import Alamofire

class AlamofireNetworkManager : NetworkInterface {
    func request(specs: RequestSpecs, completionHandler: @escaping ((AnyObject?, NSError?) -> Void)) {
        let request = Alamofire.request( specs.url, method: HTTPMethod.get, parameters: specs.params, encoding: URLEncoding.default, headers: nil)
        request.responseJSON { response in
            if let requestURL = response.request?.url?.absoluteString {
                print("Request - \(requestURL)")
            }
            completionHandler(response.result.value as AnyObject, (response.result.error as NSError?))
        }
    }
}
