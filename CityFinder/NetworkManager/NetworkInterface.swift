//
//  NetworkInterface.swift
//  CityFinder
//
//  Created by Marwa Salem on 3/17/18.
//  Copyright © 2018 marwaSalem. All rights reserved.
//

import Foundation

struct RequestSpecs {
    let params : [String: AnyObject]
    let url: String
    // TO DO: more properies could be further added to handle different http method types, encoding and headers when needed.
}

protocol NetworkInterface {
    func request(specs: RequestSpecs, completionHandler: @escaping ((AnyObject?, NSError?) -> Void))
}
