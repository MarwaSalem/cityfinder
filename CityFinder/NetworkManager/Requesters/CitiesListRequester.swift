//
//  CitiesListRequester.swift
//  CityFinder
//
//  Created by Marwa Salem on 3/17/18.
//  Copyright © 2018 marwaSalem. All rights reserved.
//

import Foundation

class CitiesListRequester {
    
    static let sharedInstance = CitiesListRequester()
    let endPoint = "cities.json?"
    let networkManager: NetworkInterface
    
    private init () {
        self.networkManager = AlamofireNetworkManager()
    }
    
    func loadCities(pageNo: Int,  completionHandler: @escaping ((AnyObject?, NSError?) -> Void)) {
        let params = [ "page" : pageNo as AnyObject]
        let specs = RequestSpecs(params: params, url: HostService.getCityListBaseUrl() + endPoint)
        networkManager.request(specs: specs, completionHandler: completionHandler)
    }
}
