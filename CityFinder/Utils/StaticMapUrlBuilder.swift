//
//  StaticMapUrlBuilder.swift
//  CityFinder
//
//  Created by Marwa Salem on 3/19/18.
//  Copyright © 2018 marwaSalem. All rights reserved.
//

import Foundation

class StaticMapUrlBuilder {
    
    static func getStaticMapUrlWith(lat: String, long: String, zoom: Int = 10, width: Int = 400, height: Int = 400) -> String {
        return "https://maps.googleapis.com/maps/api/staticmap?center=\(lat),\(long)&zoom=\(zoom)&size=\(width)x\(height)"
    }
}
