//
//  AppDelegate.swift
//  CityFinder
//
//  Created by Marwa Salem on 3/17/18.
//  Copyright © 2018 marwaSalem. All rights reserved.
//

import UIKit
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        GMSServices.provideAPIKey(HostService.getGoogleMapsAPIKey())
        window = UIWindow(frame: UIScreen.main.bounds)
        let rootViewController = CitiesListWireframe().navigationController
        window?.rootViewController = rootViewController
        window?.makeKeyAndVisible()
        return true
    }

}

