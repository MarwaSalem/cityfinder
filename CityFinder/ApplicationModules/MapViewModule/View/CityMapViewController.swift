//
//  CityMapViewController.swift
//  CityFinder
//
//  Created by Marwa Salem on 3/18/18.
//  Copyright © 2018 marwaSalem. All rights reserved.
//

import UIKit
import GoogleMaps

class CityMapViewController: UIViewController {

    var model: City?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        showMap()
    }

    static func instantiateViewController() -> CityMapViewController? {
        let storyboard = UIStoryboard(name: "CityMap", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "CityMapViewController") as? CityMapViewController
        return viewController
    }
    
    func configureView() {
        self.title = model?.name
    }
    
    func showMap() {
        guard let unwrappedModel = model,
            let lat = Double(unwrappedModel.location.latitude),
            let long = Double(unwrappedModel.location.longitude) else { return }
        
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 6.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        marker.title = unwrappedModel.name
        marker.snippet = unwrappedModel.country
        marker.map = mapView
    }
}
