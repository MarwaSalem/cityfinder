//
//  CityMapWireframe.swift
//  CityFinder
//
//  Created by Marwa Salem on 3/18/18.
//  Copyright © 2018 marwaSalem. All rights reserved.
//

import UIKit

class CityMapWireframe {
    var genericController: CityMapViewController?
    var parentNavigation: UINavigationController?
    
    init(parentNavigation: UINavigationController?) {
        self.parentNavigation = parentNavigation
        genericController = CityMapViewController.instantiateViewController()
    }
    
    func show(city: City) {
        guard let controller = genericController else { return }
        controller.model = city
        parentNavigation?.show(controller, sender: nil)
    }
    
    func popToRoot() {
        _ = parentNavigation?.popToRootViewController(animated: true)
    }
}
