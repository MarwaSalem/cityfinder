//
//  CityListPersitor.swift
//  CityFinder
//
//  Created by Marwa Salem on 3/21/18.
//  Copyright © 2018 marwaSalem. All rights reserved.
//

import Foundation

class CityListPersistor {
    
    let fileName = "Cities.plist"
    static let sharedInstance = CityListPersistor()
    
    private init() { }
    
    func saveCitiesList(_ rawCitiesList: [[String: AnyObject]]) {
        let success = NSKeyedArchiver.archiveRootObject(rawCitiesList, toFile: getCitiesListPath().path)
        if !success { print("Couldn't write file") }
    }
    
    func getCitiesList() -> [[String: AnyObject]]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: getCitiesListPath().path) as? [[String: AnyObject]] ?? nil
    }
    
    private func getCitiesListPath() -> URL {
        return getDocumentsDirectoryPath().appendingPathComponent(fileName)
    }
    
    private func getDocumentsDirectoryPath() -> URL {
       return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
}
