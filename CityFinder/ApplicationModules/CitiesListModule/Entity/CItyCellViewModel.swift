//
//  CItyCellViewModel.swift
//  CityFinder
//
//  Created by Marwa Salem on 3/18/18.
//  Copyright © 2018 marwaSalem. All rights reserved.
//

import Foundation

struct CitiesViewModel {
    let identifier = "CityTableViewCell"
    let citiesList: [CityCellViewModel]
}

struct CityCellViewModel {
    let cityName: String
    let country: String
    let mapUrl: String
}

extension CityCellViewModel {
    init(by city: City) {
        cityName = city.name
        country = city.country
        mapUrl = StaticMapUrlBuilder.getStaticMapUrlWith(lat: city.location.latitude, long: city.location.longitude)
    }
}
