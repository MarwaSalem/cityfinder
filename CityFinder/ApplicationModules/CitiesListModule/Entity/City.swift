//
//  City.swift
//  CityFinder
//
//  Created by Marwa Salem on 3/17/18.
//  Copyright © 2018 marwaSalem. All rights reserved.
//

import Foundation

struct CityEntityKeys {
    static let name = "name"
    static let id = "_id"
    static let country = "country"
    static let location = "coord"
}

struct LocationKeys {
    static let longitude = "lon"
    static let latitude = "lat"
}

struct Location {
    let latitude: String
    let longitude: String
    var dictionaryRepesentation: [String: AnyObject]  {
        get {
            var dict = [String: AnyObject]()
            dict[LocationKeys.latitude] = latitude as AnyObject
            dict[LocationKeys.longitude] = longitude as AnyObject
            return dict
        }
    }
}

extension Location {
    init(by dict: [String: AnyObject]) {
        latitude = dict[LocationKeys.latitude] as? String ?? "0"
        longitude = dict[LocationKeys.longitude] as? String ?? "0"
    }
}

struct City {
    let name: String
    let id: String
    let country: String
    let location: Location
    var dictionaryRepesentation: [String: AnyObject] {
        get {
            var dict = [String: AnyObject]()
            dict[CityEntityKeys.name] = name as AnyObject
            dict[CityEntityKeys.id] = id as AnyObject
            dict[CityEntityKeys.country] = country as AnyObject
            dict[CityEntityKeys.location] = location.dictionaryRepesentation as AnyObject
            return dict
        }
    }
}

extension City {
    init(by dict: [String: AnyObject]) {
        name = dict[CityEntityKeys.name] as? String ?? ""
        id = dict[CityEntityKeys.id] as? String ?? ""
        country = dict[CityEntityKeys.country] as? String ?? ""
        location = Location(by: dict[CityEntityKeys.location] as? [String: AnyObject] ?? [:] )
    }
}

