//
//  CitiesListWireframe.swift
//  CityFinder
//
//  Created by Marwa Salem on 3/17/18.
//  Copyright © 2018 marwaSalem. All rights reserved.
//

import UIKit

class CitiesListWireframe {
    var navigationController: UINavigationController?
    var viewController: CitiesListViewController?
    
    init() {
        viewController = CitiesListViewController.instantiateViewController()
        viewController?.presenter = getPresenter()
        if let viewController = viewController {
            navigationController = UINavigationController(rootViewController: viewController)
        }
    }
    
    fileprivate func getPresenter() -> CitiesListPresenter {
        let presenter = CitiesListPresenter(wireFrame: self)
        let interactor = CitiesListInteractor(presenter: presenter)
        presenter.interactor = interactor
        return presenter
    }
    
    func showCityMap(model: City) {
        CityMapWireframe(parentNavigation: navigationController).show(city: model)
    }
}

