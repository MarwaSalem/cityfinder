//
//  CitiesListPresenter.swift
//  CityFinder
//
//  Created by Marwa Salem on 3/17/18.
//  Copyright © 2018 marwaSalem. All rights reserved.
//

import Foundation

class CitiesListPresenter {
    let wireFrame: CitiesListWireframe
    var interactor: CitiesListInteractor?
    let  view: CitiesListViewControllerInput?
    var citiesViewModel = CitiesViewModel(citiesList: [])
    var cities = [City]()
    var filteredCities = [City]()
    var prevPrefix = ""
    
    init(wireFrame: CitiesListWireframe) {
        self.wireFrame = wireFrame
        view = wireFrame.viewController
    }
    
    func updateView() {
        updateViewModel()
        self.view?.reloadView()
    }
    
    func updateViewModel() {
        let citiesCellsList = filteredCities.map(CityCellViewModel.init)
        self.citiesViewModel = CitiesViewModel(citiesList: citiesCellsList)
    }
    
    func resetFilters() {
        filteredCities = cities
        updateView()
    }
    
    private func updateDataWith(citiesList: [City]) {
        self.cities = citiesList
        self.filteredCities = citiesList
        self.updateView()
    }
}

extension CitiesListPresenter: CitiesListViewControllerOutput {

    func didLoadView() {
        guard let unwrappedInteractor = interactor else { return }
        let savedCityList = unwrappedInteractor.getCitiesList()
        if savedCityList.isEmpty {
            unwrappedInteractor.loadCities(pageNo: 1) { (citiesList, errorString) in
                self.updateDataWith(citiesList: citiesList)
            }
        } else {
             self.updateDataWith(citiesList: savedCityList)
        }
    }

    func getCitiesListViewModel() -> CitiesViewModel {
        return citiesViewModel
    }
    
    func didSelectCellAtIndex(_ index: Int) {
        wireFrame.showCityMap(model: filteredCities[index])
    }
    
    func didResetSearchText() {
        resetFilters()
    }
    
    func filterCitiesBy(searchText: String?) {
        guard let text = searchText?.lowercased() else { return }
        if !text.hasPrefix(prevPrefix) {
            resetFilters()
        }
        filteredCities = filteredCities.filter{ $0.name.lowercased().hasPrefix(text) }
        prevPrefix = text
        updateView()
    }
    
}
