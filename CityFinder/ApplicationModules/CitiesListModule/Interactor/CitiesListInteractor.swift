//
//  CityListInteractor.swift
//  CityFinder
//
//  Created by Marwa Salem on 3/17/18.
//  Copyright © 2018 marwaSalem. All rights reserved.
//

import Foundation

class CitiesListInteractor {
    
    weak var presenter:  CitiesListPresenter?
    var citiesList = [City]()
    
    init(presenter: CitiesListPresenter) {
        self.presenter = presenter
    }
    
    func getCitiesList() -> [City] {
        guard let list = CityListPersistor.sharedInstance.getCitiesList() else { return [] }
        return self.responseToCityList(response: list)
    }
    
    func loadCities(pageNo: Int , completionHandler: @escaping (( [City], String?) -> Void)) {
        CitiesListRequester.sharedInstance.loadCities(pageNo: pageNo) {(response, error) in
            if let error = error {
                let errorMsg = error.localizedDescription
                print("An error occured \(errorMsg)")
            }
            print("Finished request page \(pageNo)")
            let cities = self.responseToCityList(response: response)
            self.citiesList.append(contentsOf: cities)
          //  if citiesList.count == 1000 {
            if cities.isEmpty {
                let sortedList = self.citiesList.sorted { $0.name.localizedCaseInsensitiveCompare($1.name) == ComparisonResult.orderedAscending }
                let rawList = sortedList.map { $0.dictionaryRepesentation }
                CityListPersistor.sharedInstance.saveCitiesList(rawList)
                completionHandler(sortedList, nil)
            } else {
                let newPageNo = pageNo + 1
                self.loadCities(pageNo: newPageNo, completionHandler: completionHandler)
            }
        }
    }
    
// Trial for concurrent requests.
    
//    func loadCities(completionHandler: @escaping (( [City], String?) -> Void)) {
//        var citiesList = [City]()
//        let myGroup = DispatchGroup()
//        for pageNo in  1..<4193  {
//            myGroup.enter()
//            CitiesListRequester.sharedInstance.loadCities(pageNo: pageNo) {(response, error) in
//                if let error = error {
//                    let errorMsg = error.localizedDescription
//                    print("An error occured \(errorMsg)")
//                }
//                print("Finished request page \(pageNo)")
//                let cities = self.responseToCityList(response: response)
//                citiesList.append(contentsOf: cities)
//                myGroup.leave()
//            }
//        }
//        myGroup.notify(queue: .main) {
//            print("Finished all requests with cities count: \(citiesList.count)")
//            completionHandler(citiesList, nil)
//        }
//    }
    
    private func responseToCityList(response: Any?) -> [City] {
        guard let rawCitiesList = response as? [[String: AnyObject]] else { return [] }
        return  rawCitiesList.map(City.init)
    }
}
