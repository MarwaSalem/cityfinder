//
//  CityTableViewCell.swift
//  CityFinder
//
//  Created by Marwa Salem on 3/17/18.
//  Copyright © 2018 marwaSalem. All rights reserved.
//

import UIKit
import AlamofireImage

class CityTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mapImageView: UIImageView!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    func configureCell(with viewModel: CityCellViewModel) {
        cityLabel.text = viewModel.cityName
        countryLabel.text = viewModel.country
        if let mapImageUrl = URL(string: viewModel.mapUrl) {
            mapImageView.af_setImage(withURL: mapImageUrl, placeholderImage: UIImage())
        }
    }
    
}
