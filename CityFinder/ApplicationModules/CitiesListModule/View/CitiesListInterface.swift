//
//  CitiesListInterface.swift
//  CityFinder
//
//  Created by Marwa Salem on 3/18/18.
//  Copyright © 2018 marwaSalem. All rights reserved.
//

import Foundation

protocol CitiesListViewControllerInput {
    func reloadView()
}

protocol CitiesListViewControllerOutput {
    func didLoadView()
    func didSelectCellAtIndex(_ index: Int)
    func didResetSearchText()
    func filterCitiesBy(searchText: String?)
    func getCitiesListViewModel() -> CitiesViewModel
}
