//
//  CitiesListViewController.swift
//  CityFinder
//
//  Created by Marwa Salem on 3/17/18.
//  Copyright © 2018 marwaSalem. All rights reserved.
//

import UIKit

class CitiesListViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var intialView: UIView!
    
    let searchController = UISearchController(searchResultsController: nil)
    var presenter: CitiesListViewControllerOutput?
    var viewModel: CitiesViewModel? {
        get {
            return presenter?.getCitiesListViewModel()
        }
    }
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.register(UINib(nibName: "CityTableViewCell" , bundle: Bundle.main), forCellReuseIdentifier: "CityTableViewCell")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        presenter?.didLoadView()
    }
    
    static func instantiateViewController() -> CitiesListViewController? {
        let storyboard = UIStoryboard(name: "CitiesList", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "CitiesListViewController") as? CitiesListViewController
        return viewController
    }
    
    private func configureView(){
        navigationController?.isNavigationBarHidden = true
        activityIndicator.startAnimating()
        configureSearchController()
    }
    
    private func configureSearchController() {
        self.navigationItem.titleView = searchController.searchBar
        searchController.searchBar.delegate = self
        searchController.searchResultsUpdater = self
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.showsCancelButton = false
        searchController.searchBar.placeholder = "Search cities"
    }
    
}

extension CitiesListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.citiesList.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let unwrappedViewModel = viewModel else {
            return UITableViewCell()
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: unwrappedViewModel.identifier, for: indexPath)
        let cityCellViewModel = unwrappedViewModel.citiesList[indexPath.row]
        (cell as? CityTableViewCell)?.configureCell(with: cityCellViewModel)
        return cell
    }
}

extension CitiesListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchController.searchBar.resignFirstResponder()
        print("Did select row at index \(indexPath.row)")
        presenter?.didSelectCellAtIndex(indexPath.row)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension CitiesListViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        presenter?.didResetSearchText()
    }
}

extension CitiesListViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        presenter?.filterCitiesBy(searchText: searchController.searchBar.text)
    }
}

extension CitiesListViewController: CitiesListViewControllerInput {
    
    func reloadView() {
        navigationController?.isNavigationBarHidden = false
        self.intialView.isHidden = true
        tableView.reloadData()
    }
}
